function Employee(
  _tk,
  _name,
  _email,
  _password,
  _birth,
  _salary,
  _position,
  _hours
) {
  this.tk = _tk;
  this.name = _name;
  this.email = _email;
  this.password = _password;
  this.birth = _birth;
  this.salary = _salary;
  this.position = _position;
  this.hours = _hours;

  this.calSalary = function () {
    switch (this.position) {
      case "Nhân viên":
        return this.salary * 1;
      case "Trưởng phòng":
        return this.salary * 1 * 2;
      case "Sếp":
        return this.salary * 1 * 3;
    }
  };

  this.empRating = function () {
    if (this.hours * 1 >= 192) {
      return "Xuất sắc";
    } else if (this.hours * 1 >= 176 && this.hours * 1 < 192) {
      return "Giỏi";
    } else if (this.hours * 1 >= 160 && this.hours * 1 < 176) {
      return "Khá";
    } else {
      return "Trung bình";
    }
  };
}
